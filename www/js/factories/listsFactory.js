app.factory('listsFactory', function($http, listsUrl) {
	var service = {};
	var url = listsUrl;	
	var lists = [];

	/*service.getLists = function() {
		return $http.get(url);
	}*/
	var getListsFromServer = function() {		
		 $http.get(url)
		 .then(function (response) {
			//console.log('cards ctrl resp', response.data);
				 lists = response.data;				 
			}, function (error) {
				// $scope.status = 'Unable to load tasks data: ' + error.message;
				// alert($scope.status);
			});
	}
	getListsFromServer();

	service.getLists = function() {		
		return lists;
	}
	service.addList = function(listName) {
		var list = {
			title: listName
		}
		
	$http.post(url, list)
		.then(function (response) {		                 
					lists.push({
						id: response.data,
						title: listName
					})
				}, function (error) {
					// $scope.status = 'Unable to add task: ' + error.message;
					// alert($scope.status);
				});        
	}
	service.removeList = function(listId) {
		console.log('removeList in factory', listId);
		_.remove(lists, {id: listId});
		var deleteUrl = url + '/'+listId;
		 $http.delete(deleteUrl);
	}
	return service;
})