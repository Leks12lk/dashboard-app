app.factory('cardFactory', function($http, cardsUrl) {
	var service = {};
	var url = cardsUrl;
	var cards = [
		{
			id: 1,
			description: 'some desc',
			listId: 22
		},
		{
			id: 2,
			description: 'some desc2',
			listId: 22
		},
		{
			id: 3,
			description: 'some desc3',
			listId: 22
		}
	];
	var cardsToRecover = [];

	var getCardsFromServer = function() {		
		 $http.get(url)
		 .then(function (response) {
			//console.log('cards ctrl resp', response.data);
				 cards = response.data;				 
			}, function (error) {
				// $scope.status = 'Unable to load tasks data: ' + error.message;
				// alert($scope.status);
			});
	}
	//getCardsFromServer();
		
		//return _.filter(cards, { listId: listId });
		
	service.getCards = function(listId) {
		console.log('listId factory', listId);
		console.log('getCards factory', cards);		
		//return _.filter(cards, { listId: listId });
		return cards;
	}
	service.createCard = function(list, cardDescription) {		
		/*var card = {
			id: _.uniqueId('card_'),
			description: cardDescription,
			listId: list.id
		}
		cards.push(card);*/
		var cardToSend = {			
			description: cardDescription,
			listId: list.id
		};
		$http.post(url, cardToSend)
		.then(function (response) {			
				cards.push(response.data);
			}, function (error) {
				// $scope.status = 'Unable to load tasks data: ' + error.message;
				// alert($scope.status);
			});
	}
	service.updateCard = function(card) {		
		$http.put(url, card);
	}
	service.deleteCard = function(card) {
		//cardsToRecover.push(_.filter(cards, {id:card.id}));
		//console.log('cards to recover', cardsToRecover);
		_.remove(cards, {id: card.id});
		var deleteUrl = url + '/'+card.id;
		$http.delete(deleteUrl);
	}
	/*service.recoverCard = function(cardId) {
		var card = _.pull(cardsToRecover, {id:cardId});
		console.log('card factory', card);
		cards.push(card);
		cardsToRecover.shift(card);
		console.log('cards', cards);
		console.log('cardsToRecover', cardsToRecover);
	}*/

	return service;

})