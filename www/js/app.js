// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('app', ['ionic', 'ui.router', 'LocalStorageModule'])

.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		if(window.cordova && window.cordova.plugins.Keyboard) {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

			// Don't remove this line unless you know what you are doing. It stops the viewport
			// from snapping when text inputs are focused. Ionic handles this internally for
			// a much nicer keyboard experience.
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if(window.StatusBar) {
			StatusBar.styleDefault();
		}
	});
});

// difene constant with baseUrl value
app.constant('baseUrl', '//tasks-dashboard.gear.host/');
app.constant('registerUrl', '//tasks-dashboard.gear.host/api/account/register');
app.constant('listsUrl', '//tasks-dashboard.gear.host/api/taskslist');
app.constant('cardsUrl', '//tasks-dashboard.gear.host/api/card');

app.config(uiRouterConfig);

function uiRouterConfig($stateProvider, $urlRouterProvider) {
	console.log('stateProvider', $stateProvider);
	$urlRouterProvider.otherwise('/');

	$stateProvider
	.state('lists', {
		url: '/',
		templateUrl: 'views/lists.html',
		controller: 'listsCtrl'
	})
	.state('view', {
		url: '/list/:id',
		templateUrl: 'views/list.html',
		controller: 'listCtrl'
	})
	.state('login', {
		url: '/login',
		templateUrl: 'views/login.html',
		controller: 'loginCtrl'
	})
	.state('signup', {
		url: '/signup',
		templateUrl: 'views/signup.html',
		controller: 'signupCtrl'
	})
}
