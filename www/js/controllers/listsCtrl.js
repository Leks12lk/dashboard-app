app.controller('listsCtrl', function($scope,listsFactory, $state) {
	var self = this;
	this.changePage = function(listId){
	    $state.go('view', {id: listId});
	  }  

	this.newListVisible = false;
	this.listName = "New list name";
	//this.lists = listsFactory.getLists();
/*	listsFactory.getLists()
		.then(function (response) {
			//$scope.lists = response.data;
			self.lists = response.data;
		}, function (error) {
			//$scope.status = 'Unable to load tasks data: ' + error.message;
			//alert($scope.status);
		});*/
	

	this.addList = function() {
		listsFactory.addList(this.listName);
		this.newListVisible = false;
	}
	this.removeList = function(listId) {
		listsFactory.removeList(listId);
	}
	this.showNewListForm = function() {
		console.log('showNewListForm ctrl');
		this.newListVisible = true;
	}
	this.hideNewListForm = function() {
		this.newListVisible = false;
	}
	this.getLists = function() {		
		return listsFactory.getLists();		
	}		
});