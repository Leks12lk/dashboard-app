'use strict';
app.controller('loginCtrl', ['$scope', '$location', 'authFactory', '$rootScope', function ($scope, $location, authFactory, $rootScope) {
    
    $scope.loginData = {
        email: "",
        password: ""
    };

    $scope.message = "";

    $scope.login = function () {
        console.log('login func');
        console.log('loginData', $scope.loginData);
        
        authFactory.login($scope.loginData).then(function (response) {
            $location.path('/');
            $rootScope.isAuthorize = true;
            console.log('$rootScope.isAuthorize', $rootScope.isAuthorize);
            /*$rootScope.logOut = authFactory.logOut;*/
        },
         function (err) {
             $scope.message = err.error_description;
         });
    };

}]);