app.controller('cardCtrl', function(cardFactory) {
	var self = this;

	this.isEditing = false;	
	this.editingCard = {};
	this.toRecoverCard = false;
	this.recoveringCardId;
	

	this.editCard = function(card) {
		this.isEditing = true;
		//this.editingCard.description = card.description;
		//this.editingCard = angular.copy(card);
		this.editingCard = card;
	}
	this.updateCard = function() {
	console.log('this.editingCard', self.editingCard);		
		//card.description = this.editingCard.description;
		cardFactory.updateCard(this.editingCard);
		/*cardFactory.getCards()
		.then(function (response) {
			console.log('cards ctrl resp', response.data);
				 self.cards = _.filter(response.data, { listId: self.editingCard.listId });
				 console.log('self.cards', self.cards);
			}, function (error) {
				// $scope.status = 'Unable to load tasks data: ' + error.message;
				// alert($scope.status);
			});*/

		this.editingCard = {};
		this.isEditing = false;
	}
	this.deleteCard = function(card) {
		this.recoveringCardId = card.id;
		this.toRecoverCard = true;
		cardFactory.deleteCard(card);
		this.isEditing = false;
		console.log('listCtrl.toRecover', this.toRecoverCard);

	}
	this.recoverCard = function() {
		cardFactory.recoverCard(this.recoveringCardId);
	}
	
});